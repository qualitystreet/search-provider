#! /bin/sh

git pull origin master

docker stop trakt

docker rm trakt

docker build -t elerion/trakt-search .

docker run --name trakt -p 4000:4000 -d elerion/trakt-search
