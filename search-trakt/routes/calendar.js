var express = require('express');
var router = express.Router();

var Trakt = require('trakt-api');
var trakt = Trakt("b84a99487f063399f3968871c32557048aea500254e544c5f9627e250b39dbaa")

var authorizedOptions = new Array('min', 'images', 'full', 'full,images', 'metadata');
var option = 'full,images';


router.get('/', function(req, res, next) {
	if(req.query.option && includes(authorizedOptions, req.query.option)){
		option = req.query.option;
	}
	var start_date;
	var day;
	var type_request;
	if(req.query.start_date){
		start_date = req.query.start_date;
	}else{
		sendError(res, "start_date");
	}
	if(req.query.day){
		day = req.query.day;
	}else{
		sendError(res, "day");
	}
	if(req.query.type_request){
		var type = req.query.type_request
	}else{
		sendError(res, "type_request");
	}
	if(type === "show"){
		trakt.calendarAllShows(start_date, day, {extended: option}).then(function(object){
			res.send(object);
		}).catch(function(err){
			console.warn(err);
			res.send(err);
		});
	}else if(type === "movie"){
		trakt.calendarAllMovies(start_date, day, {extended: option}).then(function(object){
			res.send(object);
		}).catch(function(err){
			console.warn(err);
			res.send(err);
		});
	}
	
});

var sendError =  function(res, object){
	var message = object + ' is empty';
	var err = {
		'statusCode': '404',
		'error': message
	};
	console.warn(err);
	res.send(err);
}

var includes = function(array, searchElement){
	var len = parseInt(array.length);
	if(len === 0){
		return false;
	}
	var k = 0;
	var currentElement;
	while(k < len){
		currentElement = array[k];
      if (searchElement === currentElement || (searchElement !== searchElement && currentElement !== currentElement)) {
        return true;
      }
      k++;
	}
	return false;
};


module.exports = router;