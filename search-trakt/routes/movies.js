var express = require('express');
var router = express.Router();


var Trakt = require('trakt-api');
var trakt = Trakt("b84a99487f063399f3968871c32557048aea500254e544c5f9627e250b39dbaa")

var authorizedOptions = new Array('min', 'images', 'full', 'full,images', 'metadata');
var option = 'full,images';
/* GET movies listing. */
router.get('/search', function(req, res, next) {
	var movie = req.query.movie;
	if(movie){
		trakt.searchMovie(movie).then(function(resMovie) {
	  		res.send(resMovie);
		}).catch(function(err) {
		  console.warn('oh noes', err);
		  res.send(err);
		});
	}else{
		var err = {
			"statusCode":404,
			"error": "movie is empty"
		}
		console.warn(err);
		res.send(err);
	}
});

/* GET popular films */
router.get('/popular', function(req, res, next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	trakt.moviePopular({extended: option}).then(function(movie){
		res.send(movie);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
	option = 'full,images';
});

router.get('/related', function(req, res, next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	var findMovie;
	if(req.query.movie){
		findMovie = req.query.movie;
	}else{
		var err = {
			"statusCode":404,
			"error": "movie is empty"
		}
		res.send(err);
	}
	trakt.movieRelated(findMovie, {extended: option}).then(function(movies){
		res.send(movies);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
	option = 'full,images';
})

router.get('/trending', function(req, res,next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	trakt.movieTrending({extended: option}).then(function(movie){
		res.send(movie);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
	option = 'full,images';
});

router.get('/find', function(req, res, next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	var findMovie;
	if(req.query.movie){
		findMovie = req.query.movie;
	}else{
		var err = {
			"statusCode":404,
			"error": "movie is empty"
		}
		res.send(err);
	}
	trakt.movie(findMovie, {extended : option}).then(function(movie){
		res.send(movie);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
	option = 'full,images';
})


var includes = function(array, searchElement){
	var len = parseInt(array.length);
	if(len === 0){
		return false;
	}
	var k = 0;
	var currentElement;
	while(k < len){
		currentElement = array[k];
      if (searchElement === currentElement || (searchElement !== searchElement && currentElement !== currentElement)) {
        return true;
      }
      k++;
	}
	return false;
};

module.exports = router;