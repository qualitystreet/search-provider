var express = require('express');
var router = express.Router();

var Trakt = require('trakt-api');
var trakt = Trakt("b84a99487f063399f3968871c32557048aea500254e544c5f9627e250b39dbaa")

var authorizedOptions = new Array('min', 'images', 'full', 'full,images', 'metadata');
var option = 'full,images';

router.get('/search', function(req, res, next){
	var serie = req.query.serie;
	if(serie){
		trakt.searchShow(serie).then(function(serie){
			res.send(serie);
		}).catch(function(err){
			console.warn(err);
			res.send(err);
		});
	}else{
		sendError(res, 'serie');
	}
	option='full,images';
});

/* GET popular series */
router.get('/popular', function(req, res, next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	trakt.showPopular({extended : option}).then(function(movie){
		res.send(movie);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
});

router.get('/related', function(req, res, next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	var serie;
	if(req.query.serie){
		serie = req.query.serie;
	}else{
		var err = {
			"statusCode":404,
			"error": "serie is empty"
		}
		res.send(err);
	}
	trakt.showRelated(serie, {extended : option}).then(function(series){
		res.send(series);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
	option = 'full,images';
})

router.get('/trending', function(req, res,next){
	if(req.query.option && includes(authorizedOptions,req.query.option)){
		option = req.query.option;
	}
	trakt.showTrending({extended : option}).then(function(movie){
		res.send(movie);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
});

router.get('/find', function(req, res, next){
	var serie = req.query.serie;
	if(req.query.option && includes(authorizedOptions, req.query.option)){
		option = req.query.option;
	}
	if(serie){
		trakt.show(serie, {extended: option}).then(function(serie){
			res.send(serie);
		}).catch(function(err){
			console.warn(err);
			res.send(err);
		});
	}else{
		sendError(res, 'serie');
	}
	option='full,images';
});

router.get('/:serie_id/seasons', function(req, res, next){
	var serie = req.params.serie_id;
	if(req.query.option && includes(authorizedOptions, req.query.option)){
		option = req.query.option;
	}
	if(serie){
		trakt.showSeasons(serie, {extended : option}).then(function(serie){
			res.send(serie);
		}).catch(function(err){
			console.warn(err);
			res.send(err);
		});
	}else{
		sendError(res, 'serie');
	}
	option='full,images';
});

router.get('/:serie_id/season/:season_id', function(req, res, next){
	var serie = req.params.serie_id;
	var season = req.params.season_id;
	if(req.query.option && includes(authorizedOptions, req.query.option)){
		option = req.query.option;
	}
	if(serie){
		if(season){
			trakt.season(serie, season, {extended: option}).then(function(season){
				res.send(season);
			}).catch(function(err){
				console.warn(err);
				res.send(err);
			});
		}else{
			sendError(res, 'season');
		}
	}else{
		sendError(res, 'serie');
	}
	option='full,images';
})

router.get('/:serie_id/season/:season_id/episode/:episode_id', function(req, res, next){
	var serie = req.params.serie_id;
	var season = req.params.season_id;
	var episode = req.params.episode_id;
	if(req.query.option && includes(authorizedOptions, req.query.option)){
		option = req.query.option;
	}
	if(serie){
		if(season){
			if(episode){	
				trakt.episode(serie, season, episode, {extended: option}).then(function(season){
					res.send(season);
				}).catch(function(err){
					console.warn(err);
					res.send(err);
				});
			}else{
				sendError(res, 'episode');
			}
		}else{
			sendError(res, 'season');
		}
	}else{
		sendError(res, 'serie');
	}
	option='full,images';
});


var sendError =  function(res, object){
	var message = object + ' is empty';
	var err = {
		'statusCode': '404',
		'error': message
	};
	console.warn(err);
	res.send(err);
}

var includes = function(array, searchElement){
	var len = parseInt(array.length);
	if(len === 0){
		return false;
	}
	var k = 0;
	var currentElement;
	while(k < len){
		currentElement = array[k];
      if (searchElement === currentElement || (searchElement !== searchElement && currentElement !== currentElement)) {
        return true;
      }
      k++;
	}
	return false;
};

module.exports = router;