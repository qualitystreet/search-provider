var express = require('express');
var router = express.Router();

var sendValue = {
	title: ' trakt SearchProvider',
	pages: {
		Movies : [
			{
				'title':'search',	
				"description": "Recherche d'un film dand la base de donnée",
				"options": [
					{
						"title":"movie",
						"description":"Film à rechercher"
					}
				]
			},
			{
				'title':'popular',
				'description':"Recupère les films popular",
				"options": [
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					}
				]
			},
			{
				'title':'trending',
				'description':'Recupère les films tendance',
				'options':[
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					}
				]
			},
			{
				'title':'find',
				'description':'Trouve un film en fonction de son id',
				'options':[
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					},
					{
						'title':'movie',
						'description':'Id du films à rechercher'
					}
				]
			}
		],
		Series: [
			{
				'title':'search',
				'description': 'Recherche d\'une série dans la base de donnée',
				'options':[
					{
						'title':'serie',
						'description':'Serie à rechercher'
					}
				]
			},
			{
				'title':'trending',
				'description':'Recupère les series tendance',
				'options':[
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					}
				]
			},
			{
				'title':'popular',
				'description':"Recupère les serie popular",
				"options": [
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					}
				]
			},
			{
				'title':'find',
				'description':'Trouve une serie en fonction de son id',
				'options':[
					{
						'title':'serie',
						'description':'Id de la série',
					}
				]
			},
			{
				'title':':serie_id/seasons',
				'description':'Récupère les saisons d\'une série',
				'options':[
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					},
					{
						'title':'paramètre serie_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id de la série'
					}
				]
			},
			{
				'title':':serie_id/season/:season_id',
				'description':'Récupère les informations detaillées (avec les épisodes) d\'une saison',
				'options':[
					{

						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					},
					{
						'title':'paramètre serie_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id de la série'
					},
					{
						'title':'paramètre season_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id de la saison'
					}
				]
			},
			{
				'title':':serie_id/season/:season_id/episode/:episode_id',
				"description":'Récupère les informations détaillés d\'un episode',
				'options':[
					{

						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					},
					{
						'title':'paramètre serie_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id de la série'
					},
					{
						'title':'paramètre season_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id de la saison'
					},
					{
						'title':'paramètre episode_id',
						'description':'Paramètre passé dans l\'url correspondant à l\'id d\'episode'
					}
				]
			}
		],
		calendars:[
			{
				"title":"/",
				"description":"Récupère le calendrier des sorties",
				"options":[
					{
						"title":"option",
						"description":"(default : full,images) niveau de détail dans le retour de la requête "
					},
					{
						"title":"type",
						"description":"type de requêtes films ou series"
					},
					{
						"title":"start_date",
						"description":"date de démarrage de la recherche"
					},
					{
						"title":"day",
						"description":"jour qui correspondra à la date de fin de la recherche"
					}
				]
			}
		]
	}
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', sendValue);
});

module.exports = router;
