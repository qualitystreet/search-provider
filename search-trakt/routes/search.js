var express = require('express');
var router = express.Router();

var Trakt = require('trakt-api');
var trakt = Trakt("b84a99487f063399f3968871c32557048aea500254e544c5f9627e250b39dbaa")

router.get("/", function(req, res, next) {
	var query;
	var type = "movie,show";
	if(req.query.query){
		query = req.query.query;
	}else{
		sendError(res, "query");
	}
	trakt.search(query, type).then(function(object){
		res.send(object);
	}).catch(function(err){
		console.warn(err);
		res.send(err);
	});
});

var sendError =  function(res, object){
	var message = object + ' is empty';
	var err = {
		'statusCode': '404',
		'error': message
	};
	console.warn(err);
	res.send(err);
}

module.exports = router;